<?php
require "header.php";
$name = $_POST["name"];
$parentid = $_POST["parent"];
if ($parentid == "")
	$parentid = null;
else
	$parentid = intval($parentid);
require_once "connect.php";
$conn = connect();
$s = $conn->prepare("insert into category(name, parentid) values(?, ?)") or die($conn->error);
$s->bind_param("si", $name, $parentid) or die($conn->error);
$s->execute() or die($conn->error);
$s->close();
$conn->close();
header("Location: main.php?message=".rawurlencode("Category added successfully"));
die();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Add Category</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
There was an error for some reason...
</body>
</html>
