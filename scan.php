<?php
require "header.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Simple Inventory - Scan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>
<?php
require_once 'menu.php';
?>
<button onclick="javascript:clearScan();">Clear</button>
<input type="text" id="scannedText">
<button onclick="javascript:doSearchCode();">Search Code</button>
<div id="match" style="display:none">
<input type="text" id="searchProd">
<button onclick="javascript:doSearchProd();">Search Product</button>
</div>
<div id="reaction"></div>
<a class="button" href="main.php">Main menu</a>

<script type="text/javascript">
function clearScan()
{
	document.getElementById("searchProd").value = "";
	document.getElementById("match").style.display = "none";
	var txtField = document.getElementById("scannedText");
	txtField.value = "";
	txtField.focus();
}

function assignCodeTo(subprodid)
{
	var code = document.getElementById("scannedText").value;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'doassigncode.php?code=' + encodeURIComponent(code) + "&spid=" + subprodid);
	xhr.send();
	xhr.onload = function()
	{
		if (xhr.status != 200) // analyze HTTP status of the response
			alert("error");
		else
		{
			try {
				var r = JSON.parse(xhr.response);
				if (r.status == "OK")
				{
					document.getElementById("match").style.display = "none";
					document.getElementById("reaction").innerHTML = "Successfully assigned code. Do the research again";
				}
				else
					alert("Error: " + r["error"]);
			}
			catch (e) {
				alert("Error while parsing response: " + xhr.response);
			}
		}
	};
	xhr.onerror = function()
	{
		alert("error");
	};
}

document.getElementById("searchProd").addEventListener("keyup", function(event) {
	// Number 13 is the "Enter" key on the keyboard
	if (event.keyCode === 13) {
		// Cancel the default action, if needed
		event.preventDefault();
		doSearchProd();
	}
});

function doSearchProd()
{
	var q = document.getElementById("searchProd").value;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'searchsubprod.php?q=' + encodeURIComponent(q));
	xhr.send();
	xhr.onload = function()
	{
		if (xhr.status != 200) // analyze HTTP status of the response
			alert("error");
		else
		{
			try {
				var r = JSON.parse(xhr.response);
				if (r.status == "OK")
				{
					var html = "";
					if (r.results.length == 0)
					{
						document.getElementById("match").style.display = "block";
						document.getElementById("reaction").innerHTML = "";
					}
					else
					{
						document.getElementById("match").style.display = "none";
						html = "<table border=1><tr><th>Product</th><th>Sub-Product</th><th>Action</th></tr>";
						for (const row of r.results) {
							html += "<tr><td>" + row.prodName + "</td><td>" + row.subProdName;
							if ((row.spDescr != null) && (row.spDescr != ""))
								html += " (" + row.spDescr + ")";
							if ((row.capacity != null) && (row.capacity != ""))
								html += " [" + row.capacity + "]";
							html += "</td><td><img src=\"available.png\" onclick=\"javascript:assignCodeTo(" + row.subProdId + ");\" onmouseover=\"\" title=\"Assign current barcode to this product\" style=\"cursor: pointer;\" width=40></td></tr>";
						}
						html += "</table>";
						document.getElementById("reaction").innerHTML = html;
					}
				}
				else
					alert("Error: " + r["error"]);
			}
			catch (e) {
				alert("Error while parsing response: " + xhr.response);
			}
		}
	};
	xhr.onerror = function()
	{
		alert("error");
	};
}

document.getElementById("scannedText").focus();
document.getElementById("scannedText").addEventListener("keyup", function(event) {
	// Number 13 is the "Enter" key on the keyboard
	if (event.keyCode === 13) {
		// Cancel the default action, if needed
		event.preventDefault();
		doSearchCode();
	}
});

function doUpdateStock(stockid, quantity)
{
	if (quantity == null)
	{
		var quantity = document.getElementById("n" + stockid).value;
		if (quantity == "")
			alert("Please enter a value in the field next to this button");
	}
	var xhr = new XMLHttpRequest();
	var url = 'doupdatestock.php?stockid=' + stockid + "&newquant=" + encodeURIComponent(quantity);
	xhr.open('GET', url);
	xhr.send();
	xhr.onload = function()
	{
		if (xhr.status != 200) // analyze HTTP status of the response
			alert("error");
		else
		{
			var response = xhr.response;
			try {
				var r = JSON.parse(response);
				if (r.status == "OK")
				{
					doSearchCode();
//					location.assign("scan.php?message=Saved%20successfully!");
				}
				else
					alert("Error: " + r.error);
			}
			catch (e) {
				alert(e);
				alert("Error while parsing response: " + response);
			}
		}
	};
	xhr.onerror = function()
	{
		alert("error");
	};
}

function doSearchCode()
{
	var code = document.getElementById("scannedText").value;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'getcodedata.php?code=' + encodeURIComponent(code));
	xhr.send();
	xhr.onload = function()
	{
		if (xhr.status != 200) // analyze HTTP status of the response
			alert("error");
		else
		{
			var response = xhr.response;
			try {
				var r = JSON.parse(response);
				if (r.status == "OK")
				{
					var html = "";
					if (r.results.length == 0)
					{
						document.getElementById("match").style.display = "block";
						document.getElementById("reaction").innerHTML = "<button><a href=\"editprod.php\">Create Product</a></button> <button><a href=\"editsubprod.php\">Create Sub-Product</a></button>";
						document.getElementById("searchProd").focus();
					}
					else
					{
						document.getElementById("match").style.display = "none";
						const width = 260;
						const height = 120;
						html = '<div style="display:inline-block;width:' + width + 'px;height:' + height + 'px"><canvas class="chartjs" id="ch' + r.pid + '" width="' + (width+2) + 'px" height="' + (height+2) + 'px"></canvas></div>';
						html += "<table border=1><tr><th>Product</th><th>Sub-Product</th><th>Loc</th><th>Expiration</th><th>Quant</th><th>Action</th></tr>";
						var subProdId = null;
						for (const row of r.results)
						{
							html += "<tr><td>" + row.prodName + "</td><td>" + row.subProdName + "</td>";
							html += "<td>" + (row.locationName == null ? "" : row.locationName) + "</td>";
							html += "<td>" + (row.stockExpDate == null ? "" : row.stockExpDate) + "</td>";
							html += "<td>" + (row.stockQuantity == null ? "None" : row.stockQuantity) + "</td><td>";
							if (row.stockId != null)
							{
								html += "<img src=\"minus1.png\" width=40 onclick=\"javascript:doUpdateStock('" + row.stockId + "', '-1');\" onmouseover=\"\" style=\"cursor: pointer;\"> ";
								html += "<img src=\"plus1.png\" width=40 onclick=\"javascript:doUpdateStock('" + row.stockId + "', '+1');\" onmouseover=\"\" style=\"cursor: pointer;\"> ";
								html += "<input type=\"text\" id=\"n" + row.stockId + "\" size=\"4\"><img src=\"nquant.png\" width=40 onclick=\"javascript:doUpdateStock('" + row.stockId + "', null);\" onmouseover=\"\" style=\"cursor: pointer;\">";
							}
							html += "</td></tr>";
							subProdId = row.subProdId;
						}
						html += "</table>";
						if (subProdId != null)
							html += "<br><a href=\"editstock.php?sprodid=" + subProdId + "\">Add stock</a>";
						document.getElementById("reaction").innerHTML = html;
						createGraph('ch' + r.pid, r.prod_history[r.pid], false);
					}
				}
				else
					alert("Error: " + r.error);
			}
			catch (e) {
				alert(e);
				alert("Error while parsing response: " + response);
			}
		}
	};
	xhr.onerror = function()
	{
		alert("error");
	};
}
</script>
</body>
</html>