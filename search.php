<?php
require "header.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Search products</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>
<?php
ini_set ('display_errors', 1);
ini_set ('display_startup_errors', 1);
error_reporting (E_ALL);

require_once 'menu.php';
$search = null;
if (isset($_GET["search"]))
	$search = $_GET["search"];
?>

<form id="searchForm" action="search.php">
<div><input type="text" name="search" id="prodsearch"<?php if ($search != null) echo " value=\"".htmlentities($search)."\""; ?>> <img src="search.png" onclick="javascript:searchResults();" onmouseover="" style="cursor: pointer;" width=25></div>
</form>
<script type="text/javascript">
function searchResults()
{
	document.getElementById("searchForm").submit();
}
</script>
<?php
require_once "connect.php";
$conn = connect();
require_once "helpers.php";

if ($search != "")
{
	$catTree = buildCategoriesTree($conn);
	$locTree = buildLocationsTree($conn);

	$search = "%$search%";
	$prodList = &buildProductListForSearch($conn, $catTree, $locTree, $search);
	showProdListTable($conn, $prodList);
}
else
	echo "Search a product";
$conn->close();
?>
<div class="button"><a href="main.php">Back to Menu</a></div>
</body>
</html>
