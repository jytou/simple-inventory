<?php
require "header.php";
/* set no barcode on sub product */
$sprodid = $_GET["sprodid"];

require_once "connect.php";
$conn = connect();

$s = $conn->prepare("update subprod set nobc=1 where id=?") or die($conn->error);
$s->bind_param("i", $sprodid) or die($conn->error);
$s->execute() or die($conn->error);
$s->close();

header("Location: toscan.php?message=".rawurlencode("Correctly assigned the no barcode indicator."));
die();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Assign No Barcode to Sub Product</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
There was an error for some reason...
</body>
</html>
