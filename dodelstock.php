<?php
require "header.php";
require_once "connect.php";
$conn = connect();
$stockid = intval($_GET["stockid"]);
$s = $conn->prepare("select p.catid, st.locid, st.quantity, sp.id, st.descr from prod p, subprod sp, stock st where st.id=? and st.sprodid=sp.id and sp.prodid=p.id") or die($conn->error);
$s->bind_param("i", $stockid) or die($conn->error);
$s->execute() or die($conn->error);
$s->bind_result($catid, $locid, $quantity, $sprodid, $descr);
$s->fetch();
$s->close();
if ($quantity > 0)
{
	$s = $conn->prepare("insert into event(descr, locid, quantity, sprodid) values (?, ?, ?, ?)") or die($conn->error);
	$removed = -$quantity;
	$s->bind_param("sidi", $descr, $locid, $removed, $sprodid);
	$s->execute() or die($conn->error);
	$s->close();
}
$s = $conn->prepare("delete from stock where id=?") or die($conn->error);
$s->bind_param("i", $stockid) or die($conn->error);
$s->execute() or die($conn->error);
$s->close();
$conn->close();
header("Location: browsecat.php?catid=$catid&message=".rawurlencode("Stock deleted."));
die();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Delete Stock</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
There was an error for some reason...
</body>
</html>
