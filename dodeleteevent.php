<?php
require "header.php";
require_once "connect.php";
$conn = connect();
$spid = intval($_GET["spid"]);
$locid = intval($_GET["locid"]);
$dte = $_GET["dte"];
$quantity = doubleval($_GET["q"]);

$s = $conn->prepare("delete from event where dte=? and sprodid=? and quantity=? and locid=?") or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
$s->bind_param("sidi", $dte, $spid, $quantity, $locid) or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
$s->execute() or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
$s->close();
$conn->close();

$result = array();
$result["status"] = "OK";
echo json_encode($result);
?>