<?php
require "header.php";
$catid = isset($_GET["catid"]) ? intval($_GET["catid"]) : null;
require_once "connect.php";
$conn = connect();
if (isset($_GET["prodid"]))
{
	$prodid = $_GET["prodid"];
	// get the product information
	$s = $conn->prepare("select p.name, p.descr, p.min_quantity, p.unit, p.catid from prod p where p.id=?") or die ($conn->error);
	$s->bind_param("i", $prodid);
	$s->execute();
	$s->bind_result($name, $descr, $min_quantity, $unit, $catid);
	$s->fetch();
	$s->close();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php if (isset($_GET["prodid"])) echo "Edit"; else echo "Add"; ?> Product</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>
<?php
require_once 'menu.php';
echo "name = ".$name."<br>";
?>
<form action="dosaveprod.php" method="post" enctype="multipart/form-data">
<?php
if (isset($prodid))
	echo "<input type=\"hidden\" name=\"prodid\" value=\"$prodid\">";
?>
<table>
<tr><td>Name</td><td><input type="text" name="name" size="50"<?php if (isset($name)) echo " value=\"".htmlspecialchars($name)."\""; ?>></td></tr>
<tr><td>Category</td><td><select name="catid">
<?php
require_once "helpers.php";
$catTree = buildCategoriesTree($conn);
showCatOptions($catTree, false, $catid);
$conn->close();
?>
</select></td></tr>
<tr><td>Minimum Stock</td><td><input type="text" name="min_quant" size="50"<?php if (isset($min_quantity) && ($min_quantity !== null)) echo " value=\"".$min_quantity."\""; ?>></td></tr>
<tr><td>Unit</td><td><input type="text" name="unit" size="50"<?php if (isset($unit) && ($unit !== null)) echo " value=\"".$unit."\""; ?>></td></tr>
<tr><td>Description</td><td><textarea name="desc" cols="50" rows="4"<?php if (isset($descr) && ($descr !== null)) echo " value=\"".htmlspecialchars($descr)."\""; ?>></textarea></td></tr>
</table>
<input type="submit" value="<?php
if (isset($_GET["prodid"]))
	echo "Edit product";
else
	echo "Create product";
?>">
</form>
<p>
<div class="button"><a href="browsecat.php?catid=<?php echo $catid; ?>">Back to Menu</a></div>
</body>
</html>
