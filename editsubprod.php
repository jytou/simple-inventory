<?php
require "header.php";
$catid = isset($_GET["catid"]) ? intval($_GET["catid"]) : null;
$prodid = isset($_GET["prodid"]) ? intval($_GET["prodid"]) : null;
require_once "connect.php";
$conn = connect();
$barcodes = array();
$nobc = 0;
if (isset($_GET["sprodid"]))
{
	$sprodid = $_GET["sprodid"];
	// get the product information
	$s = $conn->prepare("select name, descr, capacity, cap_unit, nobc from subprod where id=?") or die ($conn->error);
	$s->bind_param("i", $sprodid);
	$s->execute() or die ($conn->error);
	$s->bind_result($name, $descr, $capacity, $unit, $nobc);
	$s->fetch();
	$s->close();

	if ($nobc !== 1)
	{
		$s = $conn->prepare("select bc, tstamp from barcode where sprodid=?") or die ($conn->error);
		$s->bind_param("i", $sprodid);
		$s->execute() or die ($conn->error);
		$barcode = $tstamp = null;
		$s->bind_result($barcode, $tstamp);
		while ($s->fetch())
			$barcodes[$barcode] = $tstamp;
		$s->close();
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit Sub-Product</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>
<script type="text/javascript">
var nbBarcodes = 0;
function removeBarcode(index)
{
	for (let i = index ; i < nbBarcodes ; i++)
		document.getElementById("txtbc" + i).value = document.getElementById("txtbc" + (i + 1)).value;
	document.getElementById("trbc" + nbBarcodes).remove();
	nbBarcodes--;
	if (nbBarcodes == 0)
	// if there is no barcode left, show the "no barcode" checkbox so that it can be checked by the user
		document.getElementById("nobcspan").style.display = "inline-block";
}

function addBarcode()
{
	let bc = prompt("Enter barcode: ");
	if (bc != null)
		createBarcode(bc, null);
}

function createBarcode(bc, tstamp)
{
	nbBarcodes++;
	let bcrowadd = document.getElementById("bcrowadd");
	let newRow = document.createElement("tr");
	newRow.id = "trbc" + nbBarcodes;
	let newBcCode = '<td><input type="text" id="txtbc' + nbBarcodes + '" name="barcode' + nbBarcodes + '" size="30" value="' + bc + '"><sup><img src="info.png"';
	if ((tstamp != null) && (tstamp != ""))
	 newBcCode += ' title="' + tstamp + '"';
	newBcCode += ' height="12"</sup></td><td><img src="remove.png" height=24 onclick="javascript:removeBarcode(' + nbBarcodes + ');return false;" title="Remove this barcode"></td>';
	newRow.innerHTML = newBcCode;
	bcrowadd.parentNode.insertBefore(newRow, bcrowadd);

	// we have just added a barcode, make sure that we con't have "no barcode"
	document.getElementById("nobcspan").style.display = "none";
	document.getElementById("nobccb").checked = false;
}

function setUnit(unit)
{
	document.getElementById("unitTxt").value = unit;
}

window.onload = (event) => {
<?php
// create existing barcodes
foreach ($barcodes as $barcode => $tstamp)
    echo "\tcreateBarcode('$barcode', '$tstamp');\n";
?>
};
</script>
<?php
require_once 'menu.php';
?>
<form action="dosavesubprod.php" method="post" enctype="multipart/form-data">
<?php
if (isset($sprodid))
	echo "<input type=\"hidden\" name=\"sprodid\" value=\"$sprodid\">";
?>
<input type="hidden" name="catid" value="<?php echo $catid ?>">
<table>
<tr><td>Name</td><td><input type="text" name="name" size="50"<?php if (isset($name) && ($name !== null)) echo " value=\"".htmlspecialchars($name)."\""; ?>></td></tr>
<tr><td>Product</td><td>
<select name="prodid">
<?php
$s = $conn->prepare("select id, name from prod order by name") or die ($conn->error);
$s->execute();
$s->bind_result($pid, $prodname);
while ($s->fetch())
	echo "<option value=$pid".($pid == $prodid ? " selected" : "").">$prodname</option>\n";
$s->close();
$conn->close();
?>
</select>
</td></tr>
<tr>
	<td>Barcodes</td>
	<td>
		<table id="bctable">
			<tr id="bcrowadd">
				<td>
					<img src="add.png" height=24 onclick="javascript:addBarcode();return false;">
					<span id="nobcspan">&nbsp;<input type="checkbox" name="nobccb" id="nobccb"<?php if ($nobc == 1) echo "checked"; ?> onclick="javascript:toggleNoBC();return false;"><label for="nobccb">No barcode</label></span>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>Capacity</td>
	<td><input type="text" name="capacity" size="8"<?php if (isset($capacity) && ($capacity !== null)) echo " value=\"$capacity\""; ?>>
 Unit <input type="text" id="unitTxt" name="unit" size="5"<?php if (isset($unit) && ($unit !== null)) echo " value=\"".htmlspecialchars($unit)."\""; ?>>
<?php
$units = array("ml", "g");
foreach ($units as $unit)
{
	echo "&nbsp;<svg viewBox='0 0 32 32' width='32' height='32' style='cursor:pointer' onclick=\"javascript:setUnit('$unit');return false;\">
	<rect x='0' y='0' width='32' height='32' rx='9' ry='9' fill='#007bff'/>
	<text x='16' y='24' font-size='16' text-anchor='middle' fill='#fff'>$unit</text>
</svg>";
}
?>
	</td>
</tr>
<tr>
	<td>Description</td>
	<td>
		<textarea name="desc" cols="50" rows="4"><?php if (isset($descr) && ($descr !== null)) echo htmlspecialchars($descr); ?></textarea>
	</td>
</tr>
</table>
<input type="submit" value="<?php
if (isset($_GET["sprodid"]))
	echo "Edit product";
else
	echo "Create product";
?>">
</form>
<p>
<div class="button"><a href="browsecat.php?catid=<?php echo $catid; ?>">Back to Menu</a></div>
</body>
</html>
