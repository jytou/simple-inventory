<?php
require "header.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>List Products that need to be scanned</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>
<?php
require_once 'menu.php';
$filter = null;
if (isset($_GET["filter"]))
	$filter = $_GET["filter"];
?>

<div><input type="text" onkeyup="javascript:keyUpOnSearch(event);" id="prodfilter"<?php if ($filter != null) echo " value=\"".htmlentities($filter)."\""; ?>> <img src="search.png" onclick="javascript:filterResults();" onmouseover="" style="cursor: pointer;" width=25></div>
<script type="text/javascript">
function keyUpOnSearch(event)
{
	if (event.keyCode === 13)
	{
		event.preventDefault();
		filterResults();
	}
}

function filterResults()
{
	var filter = document.getElementById("prodfilter").value;
	if (filter == "")
		location.assign("toscan.php");
	else
		location.assign("toscan.php?filter=" + encodeURIComponent(filter));
}
</script>
<table>
<tr><th>Product</th><th>Sub Product</th><th>Action</th></tr>
<?php
require_once "connect.php";
$conn = connect();
require_once "helpers.php";

$sql = "SELECT p.name, p.id, p.descr, sp.name, sp.id, sp.capacity, sp.cap_unit, sp.descr FROM prod p, subprod sp where sp.nobc=0 and sp.prodid=p.id and sp.id not in (select distinct sprodid from barcode)";
$orderby = " order by p.name, sp.name";
if ($filter == null)
	$request = $sql.$orderby;
else
{
	$filter = $conn->real_escape_string($filter);
	$addFilter = " and (p.name like '%%%s%%' or sp.name like '%%%s%%' or p.descr like '%%%s%%' or sp.descr like '%%%s%%')";
	$request = sprintf($sql.$addFilter.$orderby, $filter, $filter, $filter, $filter);
}
$s = $conn->prepare($request) or die($conn->error);
$s->execute();
$s->bind_result($prodName, $prodid, $prodDescr, $subProdName, $sprodid, $spCapacity, $spUnit, $spDescr);
while ($s->fetch())
{
	echo "<tr><td>$prodName";
	if ($prodDescr != null)
		echo " [$prodDescr]";
	echo "</td><td>$subProdName";
	if ($spCapacity != null)
	{
		echo " (".$spCapacity;
		if ($spUnit != null)
			echo " ".$spUnit;
		echo ")";
	}
	if ($spDescr != null)
		echo "[$spDescr]";
	echo " <a href=\"dosetnospbc.php?sprodid=$sprodid\"><img src=\"nobc.png\" height=24></a>";
	echo "</td><td>";
	echo "<input type=\"text\" id=\"c$sprodid\" onkeyup=\"javascript:keyUpOnTxt(event, $sprodid)\"> <img src=\"available.png\" onclick=\"javascript:assignCodeTo($sprodid);\" onmouseover=\"\" style=\"cursor: pointer;\" width=40>";
	echo "</td></tr>";
}
$s->close();
$conn->close();
?>
</table>
<script type="text/javascript">
function keyUpOnTxt(event, sprodid)
{
	if (event.keyCode === 13)
	{
		event.preventDefault();
		assignCodeTo(sprodid);
	}
}

function assignCodeTo(subprodid)
{
	var code = document.getElementById("c" + subprodid).value;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'doassigncode.php?code=' + encodeURIComponent(code) + "&spid=" + subprodid);
	xhr.send();
	xhr.onload = function()
	{
		if (xhr.status != 200) // analyze HTTP status of the response
			alert("error");
		else
		{
			try {
				var r = JSON.parse(xhr.response);
				if (r.status == "OK")
				{
					location.assign("toscan.php?message=Successfully%20Assigned");
				}
				else
					alert("Error: " + r["error"]);
			}
			catch (e) {
				alert("Error while parsing response: " + xhr.response);
			}
		}
	};
	xhr.onerror = function()
	{
		alert("error");
	};
}

</script>
<div class="button"><a href="main.php">Back to Menu</a></div>
</body>
</html>
