<?php
require "header.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Simple Inventory</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>

<?php
require_once 'menu.php';
//if (isset($_GET["message"]))
//	echo htmlentities(rawurldecode($_GET["message"]))."<br>";
?>
<div class="button"><a href="createshopping.php">Create New Shopping</a></div>
<br>
<div class="button"><a href="createcat.php">Create Category</a></div>
<br>
<div class="button"><a href="browsecat.php">Browse Categories</a></div>
<br>
<div class="button"><a href="createloc.php">Create Location</a></div>
<br>
<div class="button"><a href="eventlist.php">Event List</a></div>
<br>
<div class="button"><a href="listprodexp.php">Products Expiring</a></div>
<br>
<div class="button"><a href="scan.php">Scan It!</a></div>
<br>
<div class="button"><a href="toscan.php">Products without barcodes</a></div>
</body>
</html>
