<style>
html {
	height: 100%;
}

body {
	margin: 0;
	background-repeat: no-repeat;
	background-attachment:fixed;
	background-color: rgba(0,0,0,1); /* Black w/ opacity */
	background-image: linear-gradient(#444444 45%, #22222 55%);
	background: -webkit-gradient(linear, left top, right bottom, from(#123), to(#321)) fixed;
	color: rgb(255,255,255);
	font-size: medium;
	text-align: center;
	vertical-align: center;
}

a:link {
	color:rgb(120, 255, 0);
}

a:visited {
	color:rgb(180, 255, 50);
}

table {
	margin-left: auto;
	margin-right: auto;
	padding: 5px;
}

td {
	padding: 3px;
	font-size: medium;
}

tr:nth-child(even) {background: #000}
tr:nth-child(odd) {background: #333}

textarea, input[type=text], input[type=number] {
	font-size: medium;
	padding: 5px;
	color: #af3;
	background-color: #000;
	border-radius: 5px;
}

select {
	color: #888;
	text-decoration: none;
	font-family: Helvetica, Arial, sans-serif;
	font-size: medium;
	text-align: center;
	padding: 0 30px;
	margin: 10px;
	line-height: 50px;
	border-radius: 20px;
	background-color: #222;
	background-image: linear-gradient(#444444 45%, #22222 55%);
	box-shadow: 0 2px 2px #888888;
	transition: color 0.3s, background-image 0.5s, ease-in-out;
}

input[type=submit], .button, button {
	color: #888;
	text-decoration: none;
	font-family: Helvetica, Arial, sans-serif;
	font-size: medium;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	padding: 0 30px;
	margin: 10px;
	line-height: 50px;
	border-radius: 20px;
	background-color: #222;
	background-image: linear-gradient(#444444 45%, #22222 55%);
	box-shadow: 0 2px 2px #888888;
	transition: color 0.3s, background-image 0.5s, ease-in-out;
}

input[type=submit]:hover, .button:hover, button:hover {
	color: white;
	text-decoration: none;
	font-family: Helvetica, Arial, sans-serif;
	font-size: medium;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	padding: 0 30px;
	margin: 10px;
	line-height: 50px;
	border-radius: 20px;
	background-image: linear-gradient(#335b71 45%, #03324c 55%);
	box-shadow: 0 2px 2px #888888;
	transition: color 0.3s, background-image 0.5s, ease-in-out;
}

.chartjs {
	border-color: gray;
	border-width: 1px;
	border-style: solid;
}
</style>

<table border=0 style="padding: 20px">
<tr>
<td><a href="createshopping.php"><img src="icons/basket.png" height=40></a></td>
<td><a href="createcat.php"><img src="icons/newcat.png" height=40></a></td>
<td><a href="browsecat.php"><img src="icons/categories.png" height=40></a></td>
<td><a href="search.php"><img src="icons/search.png" height=40></a></td>
<td><a href="createloc.php"><img src="icons/newloc.png" height=40></a></td>
<td><a href="eventlist.php"><img src="icons/events.png" height=40></a></td>
<td><a href="listprodexp.php"><img src="icons/expiring.png" height=40></a></td>
<td><a href="showgraphs.php"><img src="icons/chart.png" height=40></a></td>
<td><a href="scan.php"><img src="icons/barcode.png" height=40></a></td>
<td><a href="toscan.php"><img src="icons/nobarcode.png" height=40></a></td>
</tr>
</table>
<?php
if (isset($_GET["message"]))
	echo htmlentities(rawurldecode($_GET["message"]))."<br>";
//echo "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.2.1/chart.umd.min.js\"></script>";
?>
<script src="chart.umd.4.2.1.min.js"></script>
<script type="text/javascript">
function createGraph(elementId, graphData, showTicks)
{
	// dates at which a change occurred [date}
	let chDates = [];
	// values for changes - same index as chDates
	let chVals = [];
	let min_quant = 0;
	for (let prop in graphData)
		if (prop == "prod_name")
			;
		else if (prop == "min_quant")
			min_quant = graphData["min_quant"];
		else if (Object.prototype.hasOwnProperty.call(graphData, prop))
		{
			chDates.push(prop);
			chVals.push(graphData[prop]);
		}
	const currentDate = new Date();
	const endDate = new Date(currentDate.getFullYear() - 1, currentDate.getMonth(), currentDate.getDate());
	let chIndex = 0;
	let curChDate = null;
	let lastVal = 0;
	if (chDates.length > 0)
	{
		curChDate = chDates[0].substring(0, "yyyy-mm-dd".length);
		lastVal = chVals[0];
	}
	let mylabels = [];
	let mydatasetdata = [];
	let dayNum = 365;
	const currentStock = lastVal;
	let stockValSinceBuy = -1;
	let stockDaysSinceBuy = -1;
	for (let d = currentDate; d >= endDate; d.setDate(d.getDate() - 1))
	{
		const year = d.getFullYear();
		const month = (d.getMonth() + 1).toString().padStart(2, '0');
		const day = d.getDate().toString().padStart(2, '0');
		const dstring = "" + year + "-" + month + "-" + day;
		if ((curChDate != null) && (curChDate >= dstring))
		// use the current change date as it is greater than the current iteration date
		{
			mylabels.unshift(curChDate);
			if (chIndex < chDates.length)
			{
				if ((stockValSinceBuy < 0) && (chVals[chIndex] < lastVal))
				// we have bought something at this date - remember we are going backward
				{
					if (dayNum > 40)
					// only take into account last buy if it was more than a month or so ago
					{
						stockValSinceBuy = lastVal;
						stockDaysSinceBuy = dayNum;
					}
				}
				lastVal = chVals[chIndex];
			}
			chIndex++;
			if (chIndex < chDates.length)
				curChDate = chDates[chIndex].substring(0, "yyyy-mm-dd".length);
			else
				curChDate = null;
		}
		else
		// use the current iteration date directly
			mylabels.unshift(dstring);
		mydatasetdata.unshift({x: dayNum--, y: lastVal});
	}
	let lineColor = currentStock <= min_quant ? "#880" : "#0f0";
	if ((stockValSinceBuy > 0) && (currentStock > min_quant))
	{
		const slope = 9 * 30 / stockDaysSinceBuy / (currentStock - min_quant) * (stockValSinceBuy - currentStock);
		const greenVal = slope < 1 ? 255 : 255 * (2 - Math.max(0, slope));
		const redVal = Math.min(255, (Math.max(1, slope) - 1) * 255);
		lineColor = "rgb(" + Math.round(redVal) + ", " + Math.round(greenVal) + ", 0)";
	}
	const bgPlugin = {
		id: 'customCanvasBackgroundColor',
		beforeDraw: (chart, args, options) => {
			const {ctx} = chart;
			ctx.save();
			ctx.globalCompositeOperation = 'destination-over';
			ctx.fillStyle = options.color || '#99ffff';
			ctx.fillRect(0, 0, chart.width, chart.height);
			ctx.restore();
		}
	};
	let data = {
		type: 'line',
		options: {
			scales: {
				y: {beginAtZero: true, ticks: {display: showTicks}},
				x: {ticks: {display: showTicks}},
				xAxis: {display: showTicks},
				yAxis: {display: showTicks}
			},
			plugins: {
				legend: {
					display: false
				},
				customCanvasBackgroundColor: {
					color: '#222',
				}
			}
		},
		data: {
			labels: mylabels,
			datasets: [{label: elementId, borderColor: lineColor, borderWidth: showTicks ? 2 : 1, pointRadius: showTicks ? 2 : 0.5, data: mydatasetdata}]
		},
		plugins: [bgPlugin],
	};
	new Chart(document.getElementById(elementId), data);
}
</script>
