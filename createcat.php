<?php
require "header.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Add Category</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>
<?php
require_once 'menu.php';
?>
<form action="docreatecat.php" method="post" enctype="multipart/form-data">
<table>
<tr><td>Name</td><td><input type="text" name="name" size="50"></td></tr>
<tr><td>Parent</td><td><select name="parent">
<?php
require_once "connect.php";
$conn = connect();
require_once "helpers.php";
$catTree = buildCategoriesTree($conn);
showCatOptions($catTree, true);
$conn->close();
?>
</select></td></tr>
</table>
<input type="submit">
</form>
<p>
<div class="button"><a href="main.php">Back to Menu</a></div>
</body>
</html>
