<?php
require "header.php";
$name = $_POST["name"];
$catid = $_POST["catid"];
$prodid = $_POST["prodid"];
$capacity = $_POST["capacity"];
$nobc = isset($_POST["nobccb"]) ? ($_POST["nobccb"] == "on" ? 1 : 0) : 0;
if (($capacity != null) && ($capacity != ""))
	$capacity = doubleval($capacity);
else
	$capacity = null;
$unit = $_POST["unit"];
$desc = $_POST["desc"];

$barcodes = array();
$i = 1;
while (isset($_POST["barcode$i"]) && ($_POST["barcode$i"] != ""))
{
	$barcodes[$_POST["barcode$i"]] = true;
	$i++;
}

require_once "connect.php";
$conn = connect();

$existingBCs = array();
if (isset($_POST["sprodid"]))
{
	$sprodid = $_POST["sprodid"];
	$s = $conn->prepare("select bc from barcode where sprodid=?") or die($conn->error);
	$s->bind_param("i", $sprodid) or die($conn->error);
	$s->execute() or die($conn->error);
	$rsBC = null;
	$s->bind_result($rsBC);
	while ($s->fetch())
		$existingBCs[$rsBC] = true;
	$s->close();

	$s = $conn->prepare("update subprod set prodid=?, name=?, capacity=?, cap_unit=?, descr=?, nobc=? where id=?") or die($conn->error);
	$s->bind_param("isdssii", $prodid, $name, $capacity, $unit, $desc, $nobc, $sprodid) or die($conn->error);
}
else
{
	// create a new one
	$s = $conn->prepare("insert into subprod(name, prodid, capacity, cap_unit, descr, nobc) values(?, ?, ?, ?, ?)") or die($conn->error);
	$s->bind_param("sidssi", $name, $prodid, $capacity, $unit, $desc, $nobc) or die($conn->error);
}
$s->execute() or die($conn->error);
if (!isset($_POST["sprodid"]))
	$sprodid = $conn->insert_id;
$s->close();

// print_r($barcodes);
// echo "<br>";
// print_r($existingBCs);

// Deal with barcodes
$s = $conn->prepare("insert into barcode(sprodid, bc) values (?, ?)") or die($conn->error);
foreach (array_keys($barcodes) as $barcode)
	if (array_key_exists($barcode, $existingBCs))
		unset($existingBCs[$barcode]);
	else
	{
		$s->bind_param("is", $sprodid, $barcode) or die($conn->error);
		$s->execute() or die($conn->error);
	}
$s->close();
$s = $conn->prepare("delete from barcode where sprodid=? and bc=?") or die($conn->error);
foreach (array_keys($existingBCs) as $barcode)
{
	$s->bind_param("is", $sprodid, $barcode) or die($conn->error);
	$s->execute() or die($conn->error);
}
$s->close();

$conn->close();
if (isset($_POST["sprodid"]))
	header("Location: browsecat.php?catid=$catid&message=".rawurlencode("Sub-Product edited successfully"));
else
	header("Location: editstock.php?catid=$catid&prodid=$prodid&sprodid=$sprodid&message=".rawurlencode("Sub-Product created successfully. Create some stock."));
die();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit Sub Product</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
There was an error for some reason...
</body>
</html>
