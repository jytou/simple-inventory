<?php
require "header.php";
$catid = intval($_POST["catid"]);
$prodid = intval($_POST["prodid"]);
$sprodid = intval($_POST["sprodid"]);
$locid = intval($_POST["locid"]);
$quantity = $_POST["quantity"];

require_once "connect.php";
$conn = connect();
require_once 'helpers.php';

//$exp_date = strtotime(strtr($_POST["expdate"], "/", "-"));
$exp_date = transformToDate($_POST["expdate"]);
$boughton = transformToDate($_POST["boughton"]);
$desc = $_POST["desc"];
updateStock($conn, $sprodid, isset($_POST["stockid"]) ? $_POST["stockid"] : null, $quantity, $locid, $boughton, $exp_date, $desc);
$conn->close();
if ($catid != 0)
	header("Location: browsecat.php?catid=$catid&message=".rawurlencode("Stock ".(isset($_POST["stockid"]) ? "edited" : "created")." successfully"));
else
	header("Location: scan.php?message=".rawurlencode("Stock created successfully"));
die();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit Stock</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<?php echo $exp_date; ?>
There was an error for some reason...
</body>
</html>
