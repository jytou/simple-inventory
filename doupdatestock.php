<?php
require "header.php";
$stockid = intval($_GET["stockid"]);
$newquant = $_GET["newquant"];
require_once "connect.php";
$conn = connect();

require_once 'helpers.php';
updateStock($conn, null, $stockid, $newquant);
$conn->close();

$result = array();
$result["status"] = "OK";
echo json_encode($result);
?>
