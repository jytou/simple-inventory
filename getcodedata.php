<?php
require "header.php";
$code = $_GET["code"];
require_once "connect.php";
require_once 'helpers.php';
$conn = connect();
$s = $conn->prepare("select
p.id, sp.id, p.name, sp.name, st.id, l.name, st.quantity, st.exp_date, sp.descr, sp.capacity, sp.cap_unit
from
barcode b straight_join subprod sp on sp.id=b.sprodid and b.bc=? straight_join prod p on sp.prodid=p.id left join stock st on st.sprodid=sp.id left join location l on st.locid=l.id
order by p.name, st.exp_date") or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
$s->bind_param("s", $code) or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
$s->execute() or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
$s->bind_result($prodid, $subProdId, $prodName, $subProdName, $stockId, $locationName, $stockQuantity, $stockExpDate, $spDescr, $capacity, $cap_unit);
$allResults = array();
$firstprodid = null;
while ($s->fetch())
{
	$line = array();
	if ($firstprodid === null)
		$firstprodid = $prodid;
	$line["stockId"] = $stockId == null ? null : intval($stockId);
	$line["subProdId"] = intval($subProdId);
	$line["prodName"] = $prodName;
	$line["subProdName"] = $subProdName;
	$line["locationName"] = $locationName;
	$line["stockQuantity"] = $stockQuantity == null ? null : doubleval($stockQuantity);
	$line["stockExpDate"] = $stockExpDate;
	$line["spDescr"] = $spDescr;
	$line["capacity"] = $capacity == null ? null : ($cap_unit == null ? $capacity : "".$capacity." ".$cap_unit);
	$allResults[] = $line;
}
$s->close();

$result = array();
if ($firstprodid !== null)
{
	$pids = array($firstprodid);
	$result["pid"] = $firstprodid;
	$result["prod_history"] = &calculateProdEventData($conn, $pids);
}

$conn->close();

$result["status"] = "OK";
$result["results"] = $allResults;
echo json_encode($result);
?>
