<?php
require "header.php";
$q = $_GET["q"];
require_once "connect.php";
require_once 'helpers.php';
$conn = connect();
$q = $conn->real_escape_string($q);
$sql = sprintf("select p.id, sp.id, p.name, sp.name, sp.descr, sp.capacity, sp.cap_unit from subprod sp, prod p where sp.prodid=p.id and (sp.name like ? or sp.descr like ? or p.name like ? or p.descr like ?) order by 2, 3");
$s = $conn->prepare($sql) or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
$search = "%$q%";
$s->bind_param("ssss", $search, $search, $search, $search);
$s->execute() or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
$prodid = $subProdId = $prodName = $subProdName = $spDescr = $capacity = $cap_unit = null;
$s->bind_result($prodid, $subProdId, $prodName, $subProdName, $spDescr, $capacity, $cap_unit);
$allResults = array();
while ($s->fetch())
{
	$line = array();
	$line["subProdId"] = intval($subProdId);
	$line["prodName"] = $prodName;
	$line["subProdName"] = $subProdName;
	$line["spDescr"] = $spDescr;
	$line["capacity"] = $capacity == null ? null : ($cap_unit == null ? $capacity : "".$capacity." ".$cap_unit);
	$allResults[] = $line;
}
$s->close();

$result = array();

$conn->close();

$result["status"] = "OK";
$result["results"] = &$allResults;
echo json_encode($result);
?>
