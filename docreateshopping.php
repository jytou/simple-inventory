<?php
require "header.php";
require_once "connect.php";
$conn = connect();
require_once 'helpers.php';

$name = $_POST["name"];
$dte = transformToDate($_POST["dte"]);

$s = $conn->prepare("insert into shopping(name, dte) values(?, ?)") or die($conn->error);
$s->bind_param("ss", $name, $dte) or die($conn->error);
$s->execute() or die($conn->error);
$s->close();
$conn->close();
header("Location: main.php?message=".rawurlencode("Shopping added successfully"));
die();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Add Location</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
There was an error for some reason...
</body>
</html>
