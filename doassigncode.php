<?php
require "header.php";
$code = $_GET["code"];
$subprodid = intval($_GET["spid"]);
require_once "connect.php";
$conn = connect();
$s = $conn->prepare("insert into barcode(sprodid, bc) values(?, ?)") or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
$s->bind_param("is", $subprodid, $code) or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
$s->execute() or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
$s->close();
$conn->close();

$result = array();
$result["status"] = "OK";
echo json_encode($result);
?>
