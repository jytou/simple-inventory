<?php
require "header.php";
$catid = isset($_GET["catid"]) ? intval($_GET["catid"]) : null;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Browse Categories</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>
<?php
require_once 'menu.php';
?>
<table border=1>
<tr>
<?php
require_once "connect.php";
$conn = connect();
require_once "helpers.php";
$cat = buildCategoriesTree($conn, $catid);

if ($cat->parent !== null)
	showCatParents($cat->parent);
echo "<td>".$cat->name."</td>";
if (count($cat->children) > 0)
{
	echo "<td>";
	foreach ($cat->children as $subcatid => $subcat)
	{
		echo "<p>";
		showCategoryLink($subcat);
		echo "</p>";
	}
	echo "</td>";
}
?>
</tr>
</table>
<?php if ($catid !== null) { ?>
<div class="button"><a href="editprod.php<?php if ($catid !== null) echo "?catid=".$catid; ?>"><img src="add.png" height=64></a></div>
<?php } ?>
<?php
$locTree = buildLocationsTree($conn);
$prodList = buildProductListForCat($conn, $catid, $locTree);
showProdListTable($conn, $prodList, $catid);
$conn->close();
?>
<div class="button"><a href="main.php">Back to Menu</a></div>
</body>
</html>
