<?php

class Category
{
	public $id;
	public $parent;
	public $name;
	public $children = array();// catId => Category

	function __construct($id, $parent, $name)
	{
		$this->id = $id;
		$this->parent = $parent;
		$this->name = $name;
	}
}

function internalBuildCatTree(&$leaf, &$cats, &$parent2cats, &$catid, $selectedcatid)
{
	$cat = new Category($catid, $leaf, $cats[$catid]["name"]);
	$leaf->children[$catid] = $cat;
	$result = ($selectedcatid == $catid ? $cat : null);
	if (array_key_exists($catid, $parent2cats))
	{
		// fill the children
		$names = array();
		foreach ($parent2cats[$catid] as $childid => $boolean)
			$names[$cats[$childid]["name"]] = $childid;
		ksort($names);
		foreach ($names as $childname => $childid)
		{
			$found = internalBuildCatTree($cat, $cats, $parent2cats, $childid, $selectedcatid);
			if ($found !== null)
				$result = $found;
		}
	}
	return $result;
}

function buildCategoriesTree(&$conn, $selectedcatid = null)
{
	$s = $conn->prepare("select parentid, id, name from category") or die ($conn->error);
	$s->execute();
	$s->bind_result($parentid, $catid, $name);
	$cats = array();
	$parent2cats = array();
	while ($s->fetch())
	{
		$cats[$catid] = array("id" => $catid, "name" => $name, "parent" => $parentid);
		if ($parentid !== null)
		{
			if (!array_key_exists($parentid, $parent2cats))
				$parent2cats[$parentid] = array();
			$parent2cats[$parentid][$catid] = true;
		}
	}
	$names = array();
	foreach ($cats as $catid => $cat)
		if ($cat["parent"] === null)
			$names[$cat["name"]] = $catid;
	ksort($names);
	$root = new Category(null, null, "Home");
	$result = $root;
	foreach ($names as $catname => $catid)
	{
		$found = internalBuildCatTree($root, $cats, $parent2cats, $catid, $selectedcatid);
		if ($found != null)
			$result = $found;
	}
	$s->close();
	return $result;
}

function getCategoryPath(&$catTree, $catid)
{
	$cat = findCategoryInTree($catTree, $catid);
	return computeCategoryPath($cat);
}

function computeCategoryPath($cat)
{
	if ($cat != null)
	{
		$path = "";
		while ($cat->parent != null)
		{
			$path = $cat->name."/".$path;
			$cat = $cat->parent;
		}
		return $path;
	}
	else
		return "";
}

function findCategoryInTree(&$catTree, $catid)
{
	if ($catTree->id == $catid)
		return $catTree;
		foreach ($catTree->children as $childid => $child)
		{
			$found = findCategoryInTree($child, $catid);
			if ($found)
				return $found;
		}
		return null;
}

class Location
{
	public $id;
	public $parent;
	public $name;
	public $descr;
	public $children = array();// locId => Location
	function __construct($id, $parent, $name, $descr)
	{
		$this->id = $id;
		$this->parent = $parent;
		$this->name = $name;
		$this->descr = $descr;
	}
}

function getLocationPath(&$locTree, $locid)
{
	$loc = findLocationInTree($locTree, $locid);
	return computeLocationPath($loc);
}

function computeLocationPath($loc)
{
	if ($loc != null)
	{
		$path = "";
		while ($loc->parent != null)
		{
			$path = $loc->descr."/".$path;
			$loc = $loc->parent;
		}
		return $path;
	}
	else
		return "";
}

function findLocationInTree(&$locTree, $locid)
{
	if ($locTree->id == $locid)
		return $locTree;
	foreach ($locTree->children as $childid => $child)
	{
		$found = findLocationInTree($child, $locid);
		if ($found)
			return $found;
	}
	return null;
}

function internalBuildLocTree(&$leaf, &$locs, &$parent2locs, &$locid, $selectedLocId)
{
	$loc = new Location($locid, $leaf, $locs[$locid]["name"], $locs[$locid]["descr"]);
	$leaf->children[$locid] = $loc;
	$result = ($selectedLocId == $locid ? $loc : null);
	if (array_key_exists($locid, $parent2locs))
	{
		// fill the children
		$names = array();
		foreach ($parent2locs[$locid] as $childid => $boolean)
			$names[$locs[$childid]["name"]] = $childid;
		ksort($names);
		foreach ($names as $childname => $childid)
		{
			$found = internalBuildLocTree($loc, $locs, $parent2locs, $childid, $selectedLocId);
			if ($found !== null)
				$result = $found;
		}
	}
	return $result;
}

function buildLocationsTree(&$conn, $selectedLocId = null)
{
	$s = $conn->prepare("select parentid, id, name, descr from location") or die($conn->error);
	$s->execute();
	$s->bind_result($parentid, $locid, $name, $descr);
	$locs = array();
	$parent2locs = array();
	while ($s->fetch())
	{
		$locs[$locid] = array("id" => $locid,"name" => $name,"parent" => $parentid, "descr" => $descr);
		if ($parentid !== null)
		{
			if (!array_key_exists($parentid, $parent2locs))
				$parent2locs[$parentid] = array();
			$parent2locs[$parentid][$locid] = true;
		}
	}
	$names = array();
	foreach ($locs as $locid => $loc)
		if ($loc["parent"] === null)
			$names[$loc["name"]] = $locid;
	ksort($names);
	$root = new Location(null, null, "Home", "Home");
	$result = $root;
	foreach ($names as $locname => $locid)
	{
		$found = internalBuildLocTree($root, $locs, $parent2locs, $locid, $selectedLocId);
		if ($found != null)
			$result = $found;
	}
	$s->close();
	return $result;
}

class Product
{
	public $id;
	public $name;
	public $desc;
	public $min_quant;
	public $unit;
	public $sub_products = array();// subProdId => SubProduct

	function __construct($id, $name, $desc, $min_quant, $unit)
	{
		$this->id = $id;
		$this->name = $name;
		$this->desc = $desc;
		$this->min_quant = $min_quant;
		$this->unit = $unit;
	}
}

class SubProduct
{
	public $id;
	public $product;
	public $capacity;
	public $cap_unit;
	public $name;
	public $description;
	public $stocks = array();// non indexed array

	function __construct($id, $product, $name, $capacity, $cap_unit, $description)
	{
		$this->id = $id;
		$this->product = $product;
		$this->name = $name;
		$this->capacity = $capacity;
		$this->cap_unit = $cap_unit;
		$this->description = $description;
	}
}

class Stock
{
	public $id;
	public $sub_product;
	public $bought_on;
	public $description;
	public $exp_date;
	public $location;
	public $locationPath;// not filled by default, but used sometimes
	public $quantity;

	function __construct($id, $sub_product, $bought_on, $description, $exp_date, $location, $quantity)
	{
		$this->id = $id;
		$this->sub_product = $sub_product;
		$this->bought_on = $bought_on;
		$this->description = $description;
		$this->exp_date = $exp_date;
		$this->location = $location;
		$this->quantity = $quantity;
	}
}

function buildProductListForCat(&$conn, $catid, &$locTree)
{
	$s = $conn->prepare("select id, catid, name, descr, min_quantity, unit from prod where catid=? order by name") or die ($conn->error);
	$s->bind_param("i", $catid);
	$s->execute();
	$s->bind_result($prodid, $catid, $name, $desc, $min_quant, $unit);
	$prods = array();
	while ($s->fetch())
	{
		$prod = new Product($prodid, $name, $desc, $min_quant, $unit);
		$prods[$prodid] = $prod;
	}
	$s->close();
	// look for sub-products
	$s = $conn->prepare("select p.id, s.id, s.name, s.capacity, s.cap_unit, s.descr from prod p, subprod s where p.catid=? and p.id=s.prodid order by s.name") or die ($conn->error);
	$s->bind_param("i", $catid);
	$s->execute();
	$s->bind_result($prodid, $subProdId, $sname, $scap, $sunit, $sdesc);
	while ($s->fetch())
	{
		$prod = $prods[$prodid];
		$sprod = new SubProduct($subProdId, $prod, $sname, $scap, $sunit, $sdesc);
		$prod->sub_products[$subProdId] = $sprod;
	}
	$s->close();
	// look for stocks
	$s = $conn->prepare("select st.id, p.id, sp.id, st.boughton, st.descr, st.exp_date, st.locid, st.quantity from prod p, subprod sp, stock st where p.catid=? and p.id=sp.prodid and st.sprodid=sp.id order by st.exp_date") or die ($conn->error);
	$s->bind_param("i", $catid);
	$s->execute();
	$s->bind_result($stockid, $prodid, $subProdId, $bought_on, $descr, $exp_date, $locid, $quantity);
	while ($s->fetch())
	{
		$prod = $prods[$prodid];
		$sprod = $prod->sub_products[$subProdId];
		$loc = findLocationInTree($locTree, $locid);
		$stock = new Stock($stockid, $sprod, $bought_on, $descr, $exp_date, $loc, $quantity);
		$stock->locationPath = computeLocationPath($loc);
		$sprod->stocks[] = $stock;
	}
	$s->close();
	return $prods;
}

function & buildProductListForSearch($conn, &$catTree, &$locTree, $search)
{
	// find the products that match the search pattern
	$s = $conn->prepare("select p.id, p.catid, p.name, p.descr, p.min_quantity, p.unit, s.id, s.name, s.capacity, s.cap_unit, s.descr from prod p, subprod s where p.id=s.prodid and (p.name like ? or s.name like ? or p.descr like ? or s.descr like ?) order by p.name, s.name") or die ($conn->error);
	$s->bind_param("ssss", $search, $search, $search, $search);
	$s->execute();
	$s->bind_result($prodid, $catid, $name, $desc, $min_quant, $unit, $subProdId, $sname, $scap, $sunit, $sdesc);
	$prods = array();
	while ($s->fetch())
	{
		if (array_key_exists($prodid, $prods))
			$prod = $prods[$prodid];
		else
		{
			$prod = new Product($prodid, $name, $desc, $min_quant, $unit);
			$prods[$prodid] = $prod;
		}
		$sprod = new SubProduct($subProdId, $prod, $sname, $scap, $sunit, $sdesc);
		$prod->sub_products[$subProdId] = $sprod;
	}
	$s->close();
	// look for stocks
	$s = $conn->prepare("select st.id, st.boughton, st.descr, st.exp_date, st.locid, st.quantity from stock st where st.sprodid=? order by st.exp_date") or die ($conn->error);
	foreach ($prods as $prodid => $prod)
	{
		foreach ($prod->sub_products as $sprodid => $sprod)
		{
			$s->bind_param("i", $sprodid);
			$s->execute();
			$s->bind_result($stockid, $bought_on, $descr, $exp_date, $locid, $quantity);
			while ($s->fetch())
			{
				$loc = findLocationInTree($locTree, $locid);
				$stock = new Stock($stockid, $sprod, $bought_on, $descr, $exp_date, $loc, $quantity);
				$stock->locationPath = computeLocationPath($loc);
				$sprod->stocks[] = $stock;
			}
		}
	}
	$s->close();
	return $prods;
}

function showCatOptions(&$catNode, $showRoot = false, $selectedcatid = null, $parents = "")
{
	if (($catNode->id !== null) || $showRoot)
		echo "<option value=\"".$catNode->id."\"".($catNode->id == $selectedcatid ? " SELECTED" : "").">".$parents.$catNode->name."</option>\n";
	$names = array();
	foreach ($catNode->children as $childid => $childNode)
		$names[$childNode->name] = $childid;
	ksort($names);
	$parents .= $catNode->name." => ";
	foreach ($names as $childname => $childid)
		showCatOptions($catNode->children[$childid], $showRoot, $selectedcatid, $parents);
}

function showLocOptions(&$locNode, $showRoot = false, $selectedLocId = null, $parents = "")
{
	if (($locNode->id !== null) || $showRoot)
		echo "<option value=\"".$locNode->id."\"".($locNode->id == $selectedLocId ? " SELECTED" : "").">".$parents.$locNode->name."</option>\n";
	$names = array();
	foreach ($locNode->children as $childid => $childNode)
		$names[$childNode->name] = $childid;
	ksort($names);
	$parents .= $locNode->name." => ";
	foreach ($names as $childname => $childid)
		showLocOptions($locNode->children[$childid], $showRoot, $selectedLocId, $parents);
}

function showCategoryLink($category)
{
	echo "<a href='browsecat.php?catid=".$category->id."'>".$category->name."</a>";
}

function showCatParents($category)
{
	if ($category->parent !== null)
		showCatParents($category->parent);
	echo "<td>";
	showCategoryLink($category);
	echo "</td>";
}

function & calculateProdEventData($conn, &$prodids)
{
	$values = array();

	$totals = array();
	$s = $conn->prepare("select sp.capacity, s.quantity from subprod sp straight_join stock s on sp.id=s.sprodid and sp.prodid=?") or die($conn->error);
	foreach ($prodids as $prodid)
	{
		$s->bind_param("i", $prodid);
		$s->execute() or die($conn->error);
		$rsCapacity = $rsQuantity = null;
		$s->bind_result($rsCapacity, $rsQuantity);
		$totals["$prodid"] = 0;
		while ($s->fetch())
		{
			if ($rsCapacity === null)
				$totals["$prodid"] += $rsQuantity;
			else
				$totals["$prodid"] += $rsQuantity * $rsCapacity;
		}
	}
	$s->close();

	$s = $conn->prepare("select sp.capacity, e.quantity, e.dte from subprod sp straight_join `event` e on sp.id=e.sprodid and sp.prodid=? and e.dte>date_sub(now(), interval 2 year) order by e.dte desc") or die($conn->error);
	foreach ($prodids as $prodid)
	{
		$total = $totals["$prodid"];
		$s->bind_param("i", $prodid);
		$s->execute() or die($conn->error);
		$rsCapacity = $rsQuantity = $rsDte = null;
		$s->bind_result($rsCapacity, $rsQuantity, $rsDte);
		$values["$prodid"] = array(date("Y-m-d H:i:s") => $total);
		// [date as string, value as double]
		$prodvals =& $values["$prodid"];
		while ($s->fetch())
		{
			if ($rsCapacity === null)
				$total += -$rsQuantity;
			else
				$total += -$rsQuantity * $rsCapacity;
			$prodvals[$rsDte] = $total;
		}
	}
	$s->close();

	$s = $conn->prepare("select name, min_quantity from prod where id=?") or die($conn->error);
	foreach ($prodids as $prodid)
	{
		$s->bind_param("i", $prodid);
		$s->execute() or die($conn->error);
		$prodName = $minQuantity = null;
		$s->bind_result($prodName, $minQuantity);
		$s->fetch();
		if (!array_key_exists("$prodid", $values))
			$values["$prodid"] = array();
		$values["$prodid"]["prod_name"] = $prodName;
		if ($minQuantity !== null)
			$values["$prodid"]["min_quant"] = $minQuantity;
	}
	$s->close();

	return $values;
}

function generateExpDateSvg($exp_date)
{
	$res = "<span title='$exp_date'>";
	$w = 40;
	$h = 10;
	$s = 2;
	$max_dur = round(365 * 1.5);
	$res .= "<svg width=\"$w\" height=\"$h\">";
	$days = round((strtotime($exp_date) - time()) / 86400);
	$res .= "<rect width=\"$w\" height=\"$h\" style=\"fill:rgb(".($days > 0 ? "100" : "255").",0,0);stroke-width:$s;stroke:rgb(100,100,100)\" />";
	if ($days > 0)
		$res .= "<rect x=\"$s\" y=\"$s\" width=\"".(max(0, min($w-$s*2, ($w-$s*2) * $days / $max_dur)))."\" height=\"".($h-$s*2)."\" style=\"fill:#0f0\"/>";
	else
		$res .= "<rect x=\"$s\" y=\"$s\" width=\"".(max(0, min($w-$s*2, ($w-$s*2) * ($max_dur+$days) / $max_dur)))."\" height=\"".($h-$s*2)."\" style=\"fill:#000\"/>";
	$res .= "</svg>";
	$res .= "</span>";
	return $res;
}

function showProdListTable($conn, $prodList, $catid=null)
{
	$width = 130;
	$height = 90;
	echo "<table border=1>";
	foreach ($prodList as $prodid => $prod)
	{
		echo "<tr>";
		echo "<td>";
		$total = 0;
		$exp_date = 'none';
		foreach ($prod->sub_products as $subProdId => $sprod)
		{
			foreach ($sprod->stocks as $stockid => $stock)
			{
				$total += $stock->quantity * $sprod->capacity;
				if (($stock->exp_date != "") && ($stock->exp_date < $exp_date))
					$exp_date = $stock->exp_date;
			}
		}
		echo "<a href=\"editprod.php?".($catid == null ? "" : "catid=$catid&")."prodid=$prodid\">".$prod->name."</a><br>";
		if ($total < $prod->min_quant)
			echo "<font color=\"#FF0000\"><strong>$total</strong></font>";
		else
			echo $total;
		if ($prod->unit !== null)
			echo $prod->unit;
		echo "/".$prod->min_quant."/".$exp_date;
		if ($exp_date != "none")
			echo "<br>".generateExpDateSvg($exp_date);
		echo "<br>";
		echo '<div style="display:inline-block;width:'.$width.'px;height:'.$height.'px"><canvas class="chartjs" id="ch'.$prodid.'" width="'.$width.'px" height="'.$height.'px"></canvas></div>';
		echo "</td><td>";
		echo "<a href=\"editsubprod.php?".($catid == null ? "" : "catid=$catid&")."&prodid=".$prod->id."\"><img src=\"add.png\" width=40></a>";
		echo "</td><td><table>";
		foreach ($prod->sub_products as $subProdId => $sprod)
		{
			echo "<tr><td><a href=\"editsubprod.php?".($catid == null ? "" : "catid=$catid&")."prodid=".$prod->id."&sprodid=".$sprod->id."\">".$sprod->name;
			if ($sprod->capacity !== null)
			{
				echo " (".$sprod->capacity;
				if ($sprod->cap_unit !== null)
					echo $sprod->cap_unit;
				echo ")";
			}
			echo "</a>";
			$totsprod = 0;
			foreach ($sprod->stocks as $index => $stock)
				$totsprod += $stock->quantity;
			echo " (".$totsprod.") ";
			echo " <a href=\"editstock.php?".($catid == null ? "" : "catid=$catid&")."sprodid=".$sprod->id."\"><img src=\"add.png\" width=30></a>";
			echo "</td><td>";
			$first = true;
			foreach ($sprod->stocks as $index => $stock)
			{
				if ($first)
					$first = false;
				else
					echo "<br>";
				$stockid = $stock->id;
				echo "<a href=\"editstock.php?".($catid == null ? "" : "catid=$catid&")."prodid=".$prod->id."&sprodid=".$sprod->id."&stockid=$stockid\">".$stock->quantity." / ";
				if ($stock->exp_date != "")
					echo $stock->exp_date;
				else if ($stock->bought_on != "")
					echo "📥&nbsp;".$stock->bought_on;
				echo " (";
				echo $stock->location->name;
				echo ") <a href=\"dodelstock.php?stockid=$stockid\"><img src=\"subtract.png\" width=30></a>";
				if ($stock->exp_date != "")
					echo "<br>".generateExpDateSvg($stock->exp_date);
			}
			echo "</td>";
		}
		echo "</table>";
		echo "</td>";
		echo "</tr>";
	}
	echo "</table>";
	$prodids = array_keys($prodList);
	if (!empty($prodids))
	{
		echo '<script type="text/javascript">
		var graphValues = JSON.parse(\'';
		echo json_encode(calculateProdEventData($conn, $prodids));
		echo '\');
		window.onload = (event) => {
		';
		foreach ($prodids as $prodid)
			echo '	createGraph("ch'.$prodid.'", graphValues["'.$prodid.'"], false);
		';
		echo '};
		</script>';
	}
}

function updateStock($conn, $sprodid, $stockid, $quantity, $locid = null, $boughton = null, $exp_date = null, $desc = null)
{
	// today's date => date of the adding event
	$curDate = date_format(new DateTime(), "Y-m-d H:i:s");
	// this is the date of the actual buying event if buying, or current date is removing
	$dteEvent = $curDate;
	if ($stockid != null)
	// we are updating an existing stock
	{
		// quantity before...
		$s = $conn->prepare("select quantity, locid, sprodid from stock where id=?") or die($conn->error);
		$s->bind_param("i", $stockid) or die($conn->error);
		$s->execute() or die($conn->error);
		$s->bind_result($previousQuantity, $oldlocid, $sprodid);
		$s->fetch();
		$s->close();

		if ($boughton === null)
		{
			// fetch the latest shopping date
			$s = $conn->prepare("select dte from shopping order by id desc limit 1") or die($conn->error);
			$s->execute() or die($conn->error);
			$shoppingDate = null;
			$s->bind_result($shoppingDate);
			$s->fetch();
			$s->close();
			$dteEvent = $shoppingDate;
		}

		if ((substr($quantity, 0, 1) == "+") || (substr($quantity, 0, 1) == "-"))
		// adding or subtracting: directly get the variation
			$variation = intval($quantity);
		else
		// setting a new quantity: compute the variation
			$variation = $quantity - $previousQuantity;
		$newQuantity = $previousQuantity + $variation;
		if ($newQuantity < 0)
			die("Cannot go to a negative quantity");
		if ($newQuantity == 0)
		// deleting full stock
		{
			$s = $conn->prepare("delete from stock where id=?") or die($conn->error);
			$s->bind_param("i", $stockid) or die($conn->error);
			// because removal from stock always happen on the current day - not a purchase!
			$dteEvent = $curDate;
		}
		else
		// updating stock quantity
		{
			if ($locid == null)
			{
				// we are only updating the quantity
				$s = $conn->prepare("update stock set quantity=? where id=?") or die($conn->error);
				$s->bind_param("di", $newQuantity, $stockid) or die($conn->error);
			}
			else
			// we are also updating the location, etc.
			{
				$s = $conn->prepare("update stock set locid=?, quantity=?, boughton=?, exp_date=?, descr=? where id=?") or die($conn->error);
				$s->bind_param("idsssi", $locid, $newQuantity, $boughton, $exp_date, $desc, $stockid) or die($conn->error);
			}
			if ($quantity < 0)
			// because removal from stock always happen on the current day - not a purchase!
				$dteEvent = $curDate;
		}
		// We do need locid to save the event
		if (($locid == null) && ($oldlocid != null))
			$locid = $oldlocid;
	}
	else
	// we are creating a new stock
	{
		if ($boughton !== null)
		// when creating new stock, the date of the event is the date the products where bought on
			$dteEvent = $boughton;
		if (substr($quantity, 0, 1) == "-")
			die("Cannot create a stock with a negative value");
		// create a new one
		$previousQuantity = 0;
		$s = $conn->prepare("insert into stock(sprodid, locid, quantity, exp_date, boughton, descr) values(?, ?, ?, ?, ?, ?)") or die($conn->error);
		$s->bind_param("iidsss", $sprodid, $locid, $quantity, $exp_date, $boughton, $desc) or die($conn->error);
		$variation = intval($quantity);
		$newQuantity = $variation;
	}
	$s->execute() or die($conn->error);
	$s->close();

	if ($variation != 0)
	{
		// create event
		$s = $conn->prepare("insert into event(descr, locid, quantity, sprodid, dte, realdte) values (?, ?, ?, ?, ?, ?)") or die($conn->error);
		$s->bind_param("sidiss", $descr, $locid, $variation, $sprodid, $dteEvent, $curDate);
		$s->execute() or die($conn->error);
		$s->close();
	}
}

function transformToDate($raw)
{
	if (($raw === null) || ($raw == ""))
		return null;
	if (strpos($raw, "/") !== FALSE)
	// We have slashes, transform them to dashes
		$raw = strtr($raw, "/", "-");
	$parts = explode("-", $raw);
	if (count($parts) != 3)
	{
		if (count($parts) == 2)
		{
			if ((strlen($parts[0]) == 2) && (strlen($parts[1]) == 4))
			// month/year
				$raw = $parts[1]."-".$parts[0]."-15";
			else if ((strlen($parts[1]) == 2) && (strlen($parts[0]) == 4))
			// year/month
				$raw = $parts[0]."-".$parts[1]."-15";
		}
		else
			die("Incorrect date: ".$raw);
	}
	else if (strlen($parts[0]) == 2)
	{
		// invert the first and last
		$first = $parts[0];
		$parts[0] = $parts[2];
		$parts[2] = $first;
		$raw = implode("-", $parts);
	}
	return $raw;
}

/**
 * Format an interval to show all existing components.
 * If the interval doesn't have a time component (years, months, etc)
 * That component won't be displayed.
 *
 * @param DateInterval $interval The interval
 *
 * @return string Formatted interval string.
 */
function format_interval(DateInterval $interval)
{
	$result = "";
	if ($interval->y) { $result .= $interval->format("%y years "); }
	if ($interval->m) { $result .= $interval->format("%m months "); }
	if ($interval->d) { $result .= $interval->format("%d days "); }
	if ($interval->h) { $result .= $interval->format("%h hours "); }
	if ($interval->i) { $result .= $interval->format("%i minutes "); }
	if ($interval->s) { $result .= $interval->format("%s seconds "); }

	return $result;
}
?>
