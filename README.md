A simple inventory web app. I created it for my personal needs, but it may be useful to others as I couldn't find anything like it.

Main features:

- simple web app, available everywhere and on any device,
- hierarchical categories,
- product / sub-product / amount of stock, location of stock,
- easy add category, product, sub-product and add/remove stocks,
- sub-product quantities (bottle volume, etc.) and stock quantities,
- date purchased and expiration date for perishable goods,
- easy barcode search to manage stock faster,
- monitor minimum amount of grams/liters/etc. for every product,
- full list of events (add/remove stock),
- shows how long the stock will hold at current flow rate.

Requirements:

A web server with php, mysql.

Installation:

- create a schema in your mysql database,
- apply the create_v1.sql script to that schema to create the necessary tables,
- copy the directory to the webserver wherever you want,
- create a connect.ini with the connection parameters to your database from the template,
- That's it!