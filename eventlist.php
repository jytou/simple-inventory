<?php
require "header.php";
$timeframe = (isset($_POST["timeframe"]) ? $_POST["timeframe"] : "1 week");
$inout = (isset($_POST["movements"]) ? intval($_POST["movements"]) : 0);
$filter = ($timeframe != "" ? " and e.realdte>DATE_SUB(CURRENT_TIMESTAMP, INTERVAL $timeframe)" : "");
if ($inout != 0)
	$filter .= " and e.quantity".($inout == 1 ? ">" : "<")."0";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Event List</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>
<?php
require_once 'menu.php';
?>
<script type="text/javascript">
function submitDateForm()
{
	document.getElementById("dateForm").submit();;
}

function deleteLine(dte, q, spid, locid)
{
	if (!confirm("Are you sure you want to delete this line at date " + dte + "?"))
		return;
	var url = 'dodeleteevent.php?dte=' + encodeURIComponent(dte) + '&spid=' + spid + "&q=" + encodeURIComponent(q) + "&locid=" + locid;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url);
	xhr.send();
	xhr.onload = function()
	{
		if (xhr.status != 200) // analyze HTTP status of the response
			alert("error");
		else
		{
			var response = xhr.response;
			try {
				var r = JSON.parse(response);
				if (r.status == "OK")
				{
					document.getElementById(dte + "/" + spid).remove();
				}
				else
					alert("Error: " + r.error);
			}
			catch (e) {
				alert(e);
				alert("Error while parsing response: " + response);
			}
		}
	};
	xhr.onerror = function()
	{
		alert("error");
	};
}
</script>
<form id="dateForm" action="eventlist.php" method="POST">
<select name="movements" onchange="javascript:submitDateForm();">
<?php
$moves = array(-1 => "Outgoing", 0 => "All", 1 => "Incoming");
foreach ($moves as $expr => $hr)
{
	echo "<option value=\"$expr\"".($expr == $inout ? " selected" : "").">$hr</option>\n";
}
?>
</select>
<select name="timeframe" onchange="javascript:submitDateForm();">
<option value="">All time</option>
<?php
$intervals = array("1 day" => "1 day", "2 day" => "2 days", "5 day" => "5 days", "1 week" => "1 week", "2 week" => "2 weeks", "1 month" => "1 month", "1 year" => "1 year", "2 year" => "2 years");
foreach ($intervals as $expr => $hr)
	echo "<option value=\"$expr\"".($expr == $timeframe ? " selected" : "").">$hr</option>\n";
?>
</select>
</form>
<table border=1>
<tr><th>Date/submitted</th><th>Quantity</th><th>Description</th><th>Location</th><th>Category</th><th>Product</th><th>Sub-Product</th><th>Actions</th></tr>
<?php
require_once "connect.php";
$conn = connect();
require_once "helpers.php";
$catTree = buildCategoriesTree($conn);
$locTree = buildLocationsTree($conn);

$s = $conn->prepare("SELECT e.dte, e.realdte, e.quantity, e.descr, c.id, l.id, p.name, s.name, s.id FROM event e, location l, subprod s, prod p, category c where p.catid=c.id and s.prodid=p.id and l.id=e.locid and e.sprodid=s.id$filter order by e.realdte desc") or die ($conn->error);
$s->execute();
$s->bind_result($eventDate, $dteSubmit, $eventQuantity, $eventDescription, $catid, $locid, $productName, $subProductName, $subProductId);
while ($s->fetch())
{
	echo "<tr id='$eventDate/$subProductId'>";
	echo "<td>$eventDate".($eventDate != $dteSubmit ? "<br><i>".$dteSubmit."</i>" : "")."</td>";
	echo "<td>$eventQuantity</td>";
	echo "<td>$eventDescription</td>";
	echo "<td>".getLocationPath($locTree, $locid)."</td>";
	echo "<td>".getCategoryPath($catTree, $catid)."</td>";
	echo "<td>$productName</td>";
	echo "<td>$subProductName</td>";
	echo "<td><img src='subtract.png' height=24 onclick=\"javascript:deleteLine('$eventDate', $eventQuantity, $subProductId, $locid);return false;\" title='Delete line'></td>";
	echo "</tr>";
}
$s->close();
$conn->close();
?>
</table>
<div class="button"><a href="main.php">Back to Menu</a></div>
</body>
</html>