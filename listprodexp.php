<?php
require "header.php";
//$catid = isset($_GET["catid"]) ? intval($_GET["catid"]) : null;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>List Products by Expiration Date</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<script type="text/javascript">
var loading = true;
function loaded()
{
	loading = false;
}
function changeMode()
{
	if (loading)
		return;
	window.location = "listprodexp.php?mode=" + document.getElementById("cbMode").value;
}
</script>
<body onload="javascript:loaded();">
<?php
require_once 'menu.php';
$mode = (isset($_GET["mode"]) ? $_GET["mode"] : "");
?>
<select id="cbMode" onchange="javascript:changeMode();">
<option value=""<?php if ($mode == "") echo " selected"; ?>>Expiration Date</option>
<option value="1"<?php if ($mode == "1") echo " selected"; ?>>Availability</option>
</select>
<?php
require_once "connect.php";
$conn = connect();
require_once "helpers.php";
if ($mode == "1")
{
	class ProdAvail
	{
		public $id;
		public $name;
		public $minQuant;
		public $stock;
		public $realQuant;
		public $consume;
		public $units = array();
		public $margin;
		function __construct($id, $name, $minQuant, $stock, $realQuant)
		{
			$this->id = $id;
			$this->name = $name;
			$this->minQuant = $minQuant;
			$this->stock = $stock == null ? 0 : $stock;
			$this->realQuant = $realQuant == null ? 0 : $realQuant;
		}
	}
	//$cat = buildCategoriesTree($conn);//, $catid);
	// get all products availability
	$s = $conn->prepare("SELECT p.id, p.name, p.min_quantity, sum(s.quantity), sum(s.quantity * sp.capacity) FROM prod p inner join subprod sp on p.id=sp.prodid left JOIN stock s on s.sprodid=sp.id group by 1") or die($conn->error);
	$s->execute();
	$s->bind_result($prodid, $prodName, $minQuant, $quantity, $realQuant);
	// [pid, ProdAvail]
	$prods = array();
	while ($s->fetch())
		$prods[$prodid] = new ProdAvail($prodid, $prodName, $minQuant, $quantity, $realQuant);
	$s->close();
	// get all products consumption
	$s = $conn->prepare("SELECT p.id, -sum(e.quantity * sp.capacity), group_concat(sp.cap_unit) FROM prod p inner join subprod sp on p.id=sp.prodid inner JOIN event e on e.sprodid=sp.id where e.dte>=date_sub(now(), interval 6 month) and e.quantity<0 group by 1") or die($conn->error);
	$s->execute();
	$s->bind_result($prodid, $quantity, $units);
	// [pid, margin]
	$margins = array();
	while ($s->fetch())
	{
		$prodAvail = $prods[$prodid];
		$prodAvail->consume = $quantity;
		if ($quantity > 0)
		{
			$prodAvail->margin = $prodAvail->realQuant / $quantity * 6 * 30;
			$margins[$prodid] = $prodAvail->margin;
			foreach (explode(",", $units) as $unit)
				$prodAvail->units[$unit] = true;
		}
	}
	$s->close();
	asort($margins);

	function rgb2html($r, $g=-1, $b=-1)
	{
		if (is_array($r) && sizeof($r) == 3)
			list($r, $g, $b) = $r;

			$r = intval($r); $g = intval($g);
			$b = intval($b);

			$r = dechex($r<0?0:($r>255?255:$r));
			$g = dechex($g<0?0:($g>255?255:$g));
			$b = dechex($b<0?0:($b>255?255:$b));

			$color = (strlen($r) < 2?'0':'').$r;
			$color .= (strlen($g) < 2?'0':'').$g;
			$color .= (strlen($b) < 2?'0':'').$b;
			return '#'.$color;
	}

	function sqr($a)
	{
		return $a * $a;
	}

	echo "<table border=1><tr><th>Product Name</td><td>Real/Min Stock</td><td>Current Stock</td><td>Consumed last 6 months</td><td>Units</td><td>Margin (days)</td></tr>";
	foreach ($margins as $prodid => $margin)
	{
		$prod = $prods[$prodid];
		echo "<tr>";
		echo "<td>".$prod->name." ($prodid)</td>";
		echo "<td>";
		if ($prod->minQuant != 0)
		{
			$r = max(0, 255 - 100 * $prod->realQuant / $prod->minQuant);
			$g = exp(-4 * sqr($prod->realQuant > 0 ? $prod->realQuant > $prod->minQuant ? 0/*0.3 * ($prod->realQuant / $prod->minQuant - 1)*/ : $prod->minQuant / $prod->realQuant - 1 : 1000)) * 255;
			$b = min(255, 255 * ($prod->realQuant / $prod->minQuant - 1));
		}
		else
		{
			$r = 220;
			$g = 150;
			$b = 0;
		}
		echo "<font color='".rgb2html($r, $g, $b)."'>";
		echo $prod->realQuant;
		echo "</font>";
		echo " / ";
		echo $prod->minQuant;
		echo "</td>";
		echo "<td>";
		echo $prod->realQuant;
		echo "</td>";
		echo "<td>".$prod->consume."</td>";
		echo "<td>".implode(",", array_keys($prod->units))."</td>";
		echo "<td>".intval($prod->margin)."</td>";
		echo "</tr>";
	}
	echo "</table>";
}
else
{
	$locTree = buildLocationsTree($conn);

	$s = $conn->prepare("SELECT c.id, l.id, p.name, p.id, p.descr, p.min_quantity, p.unit, sp.name, sp.id, sp.capacity, sp.cap_unit, sp.descr, st.quantity, st.exp_date, st.id, st.boughton, st.descr FROM stock st, prod p, subprod sp, location l, category c where exp_date is not null and st.sprodid=sp.id and sp.prodid=p.id and st.locid=l.id and p.catid=c.id order by exp_date limit 100") or die($conn->error);
	$s->execute();
	$s->bind_result($catid, $locid, $prodName, $prodid, $prodDescr, $prodMinQuant, $prodUnit, $subProdName, $sprodid, $spCapacity, $spUnit, $spDescr, $quantity, $exp_date, $stockid, $boughton, $stDescr);
	$prodlist = array();
	while ($s->fetch())
	{
		$prod = new Product($prodid, $prodName, $prodDescr, $prodMinQuant, $prodUnit);
		$sprod = new SubProduct($sprodid, $prod, $subProdName, $spCapacity, $spUnit, $spDescr);
		$prod->sub_products[] = $sprod;
		$stock = new Stock($stockid, $sprod, $boughton, $stDescr, $exp_date, findLocationInTree($locTree, $locid), $quantity);
		$sprod->stocks[] = $stock;
		$prodlist[] = $prod;
	}
	$s->close();
	showProdListTable($prodlist);
}
$conn->close();
?>
<div class="button"><a href="main.php">Back to Menu</a></div>
</body>
</html>
