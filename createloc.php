<?php
require "header.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Add Location</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>
<?php
require_once 'menu.php';
?>
<form action="docreateloc.php" method="post" enctype="multipart/form-data">
<table>
<tr><td>Name</td><td><input type="text" name="name" size="50"></td></tr>
<tr><td>Parent</td><td><select name="parent">
<?php
require_once "connect.php";
$conn = connect();
require_once "helpers.php";
$locTree = buildLocationsTree($conn);
showLocOptions($locTree, true);
$conn->close();
?>
</select></td></tr>
<tr><td>Description</td><td><textarea name="desc" cols="50" rows="4"></textarea></td></tr>
</table>
<input type="submit">
</form>
<p>
<div class="button"><a href="main.php">Back to Menu</a></div>
</body>
</html>
