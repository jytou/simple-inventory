<?php
require "header.php";
$name = $_POST["name"];
$catid = $_POST["catid"];
$min_quant = $_POST["min_quant"];
if (($min_quant != null) && ($min_quant != ""))
	$min_quant = doubleval($min_quant);
else
	$min_quant = null;
$unit = $_POST["unit"];
if ($unit === "")
	$unit = null;
$desc = $_POST["desc"];
if ($desc === "")
	$desc = null;
if (isset($_POST["prodid"]))
	$prodid = intval($_POST["prodid"]);
else
	$prodid = null;
require_once "connect.php";
$conn = connect();
if ($prodid == null)
{
	$s = $conn->prepare("insert into prod(catid, name, min_quantity, unit, descr) values(?, ?, ?, ?, ?)") or die($conn->error);
	$s->bind_param("isdss", $catid, $name, $min_quant, $unit, $desc) or die($conn->error);
}
else
{
	$s = $conn->prepare("update prod set catid=?, name=?, min_quantity=?, unit=?, descr=? where id=?") or die($conn->error);
	$s->bind_param("isdssi", $catid, $name, $min_quant, $unit, $desc, $prodid) or die($conn->error);
}
$s->execute() or die($conn->error);
if ($prodId === null)
	$createdId = $conn->insert_id;
$s->close();
$conn->close();
if ($prodid === null)
{
	header("Location: editsubprod.php?catid=$catid&prodid=$createdId&message=".rawurlencode("Product $prodid edited successfully. Create a subproduct."));
}
else
	header("Location: browsecat.php?catid=$catid&message=".rawurlencode("Product $prodid edited successfully"));
die();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Save Product</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
There was an error for some reason...
</body>
</html>
