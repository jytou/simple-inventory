<?php
require "header.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Graphs</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>
<?php
ini_set ('display_errors', 1);
ini_set ('display_startup_errors', 1);
error_reporting (E_ALL);

require_once 'menu.php';
require_once "connect.php";
require_once 'helpers.php';
$conn = connect();
$prodid = 36;

echo '<script type="text/javascript">
var graphValues = JSON.parse(\'';
$prodids = array($prodid);
echo json_encode(calculateProdEventData($conn, $prodids));
echo '\');
window.onload = (event) => {
	createGraph("ch'.$prodid.'", graphValues["'.$prodid.'"], false);
};
</script>';
$width = 130;
$height = 90;
echo '<div width="'.$width.'px" height="'.$height.'px" style="width:'.$width.'px;height:'.$height.'px"><canvas class="chartjs" id="ch'.$prodid.'" width="'.$width.'" height="'.$height.'"></canvas></div>';

$conn->close();
?>

</body>
