<?php
require "header.php";
$catid = isset($_GET["catid"]) ? intval($_GET["catid"]) : null;
$sprodid = isset($_GET["sprodid"]) ? intval($_GET["sprodid"]) : null;
require_once "connect.php";
$conn = connect();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit Stock</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="icon.png"/>
</head>
<body>
<script type="text/javascript">
function setQuantity(q)
{
	document.getElementById("quantTxt").value = q;
}

var curDay = -1;
var curMonth = -1;

var curDest = null;
function showDatePicker(dest)
{
	curDest = dest;
	setDay(-1);
	setMonth(-1);
	document.getElementById("datePickerBackgroundDiv").style.display = "block";
	document.getElementById("datePicker").style.display = "block";
}

function hideDatePicker()
{
	document.getElementById("datePickerBackgroundDiv").style.display = "none";
	document.getElementById("datePicker").style.display = "none";
}

function setDay(d)
{
	if (curDay != -1)
		document.getElementById("day" + curDay).setAttribute("fill", "#fff");
	if (d != -1)
		document.getElementById("day" + d).setAttribute("fill", 'lightgreen');
	curDay = d;
}

function setMonth(m)
{
	if (curMonth != -1)
		document.getElementById("month" + curMonth).setAttribute("fill", "#fff");
	if (m != -1)
		document.getElementById("month" + m).setAttribute("fill", 'lightgreen');
	curMonth = m;
}

function setYear(y)
{
	if (curMonth == -1)
		return;
	let dte = (curDay == -1 ? "" : ('0' + curDay).slice(-2) + "/");
	dte += ('0' + curMonth).slice(-2) + "/" + y;
	document.getElementById(curDest).value = dte;
	hideDatePicker();
}
</script>
<div style="display:none;position:fixed;left:0;top:0;width:100%;height:100%;max-height:100%;background-color:rgba(0,0,0,0.4);z-index:10" id="datePickerBackgroundDiv" onclick="javascript:hideDatePicker();return false;"></div>
<div style="display:none;left:50%;top:50%;width:600px;height:800px;position:fixed;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);overflow: auto;background-color: #777;z-index:100" id="datePicker">
<table>
<tr><td>Day</td><td>
<?php
for ($i = 1; $i <= 31; $i++)
	echo "<svg viewBox='0 0 64 64' width='64' height='64' style='cursor:pointer' onclick=\"javascript:setDay($i);return false;\">
	<rect x='0' y='0' width='64' height='64' rx='9' ry='9' fill='#007bff'/>
	<text id='day$i' x='32' y='44' font-size='32' text-anchor='middle' fill='#fff'>$i</text>
</svg> ";
?></td></tr>
<tr><td>Month</td><td>
<?php
for ($i = 1; $i <= 12; $i++)
	echo "<svg viewBox='0 0 64 64' width='64' height='64' style='cursor:pointer' onclick=\"javascript:setMonth($i);return false;\">
	<rect x='0' y='0' width='64' height='64' rx='9' ry='9' fill='#007bff'/>
	<text id='month$i' x='32' y='44' font-size='32' text-anchor='middle' fill='#fff'>$i</text>
</svg> ";
?></td></tr>
<tr><td>Year</td><td>
<?php
for ($i = 2023; $i <= 2040; $i++)
	echo "<svg viewBox='0 0 128 64' width='128' height='64' style='cursor:pointer' onclick=\"javascript:setYear($i);return false;\">
	<rect x='0' y='0' width='128' height='64' rx='9' ry='9' fill='#007bff'/>
	<text x='64' y='44' font-size='32' text-anchor='middle' fill='#fff'>$i</text>
</svg> ";
?></td></tr>
</table>
</div>

<?php
require_once 'menu.php';
if (isset($_GET["stockid"]))
{
	$stockid = $_GET["stockid"];
	// get the product information
	$s = $conn->prepare("select locid, quantity, exp_date, boughton, descr from stock where id=?") or die ($conn->error);
	$s->bind_param("i", $stockid);
	$s->execute();
	$s->bind_result($locid, $quantity, $exp_date, $boughton, $descr);
	$s->fetch();
	$s->close();
}
else
{
	$s = $conn->prepare("select name, dte from shopping order by id desc limit 1") or die($conn->error);
	$s->execute();
	$s->bind_result($shoppingName, $boughton);
	$s->fetch();
	$s->close();
	echo "<center><h1>Using shopping $shoppingName on $boughton</h1></center>";
}
?>
<form action="dosavestock.php" method="post" enctype="multipart/form-data">
<?php
if (isset($stockid))
	echo "<input type=\"hidden\" name=\"stockid\" value=\"$stockid\">";
?>
<table>
<tr><td>Product</td><td colspan=2>
<?php
$s = $conn->prepare("select p.id, p.name, sp.name from prod p, subprod sp where sp.id=? and sp.prodid=p.id") or die ($conn->error);
$s->bind_param("i", $sprodid);
$s->execute();
$s->bind_result($prodid, $prodname, $sprodname);
if ($s->fetch())
	echo $prodname." / ".$sprodname;
$s->close();
?>
</td></tr>
<tr><td>Location</td><td colspan=2><select name="locid">
<?php
require_once "helpers.php";
$locTree = buildLocationsTree($conn);
showLocOptions($locTree, false, isset($locid) ? $locid : null);
$conn->close();
?>
</select></td></tr>
<tr><td>Quantity</td><td><input type="text" name="quantity" id="quantTxt" size="12"<?php if (isset($quantity) && ($quantity !== null)) echo " value=\"$quantity\""; ?>></td>
<td>
<?php
for ($i = 1; $i <= 10; $i++)
{
	echo "<svg viewBox='0 0 30 30' width='30' height='30' style='cursor:pointer' onclick=\"javascript:setQuantity('$i');return false;\">
	<rect x='0' y='0' width='30' height='30' rx='4' ry='4' fill='#007bff'/>
	<text x='16' y='24' font-size='24' text-anchor='middle' fill='#fff'>$i</text>
</svg>&nbsp;";
}
?></td>
</tr>
<tr><td>Bought on</td><td><input type="text" name="boughton" id="boughton" size="12"<?php if (isset($boughton) && ($boughton !== null)) echo " value=\"".$boughton."\""; ?>></td>
<td style="text-align:left">
<img src="icons/datepicker.png" width=32 style="cursor:pointer" onclick="javascript:showDatePicker('boughton');return false;">
</td>
</tr>
<tr><td>Expires on</td><td><input type="text" pattern="[0-9/-]*" name="expdate" id="expdate" size="12"<?php if (isset($exp_date) && ($exp_date !== null)) echo " value=\"".$exp_date."\""; ?>></td>
<td style="text-align:left">
<img src="icons/datepicker.png" width=32 style="cursor:pointer" onclick="javascript:showDatePicker('expdate');return false;">
</td>
</tr>
<tr><td>Description</td><td colspan=2><textarea name="desc" cols="50" rows="4"><?php if (isset($descr) && ($descr !== null)) echo htmlspecialchars($descr); ?></textarea></td></tr>
</table>
<input type="hidden" name="sprodid" value="<?php echo $sprodid ?>">
<input type="hidden" name="prodid" value="<?php echo $prodid ?>">
<input type="hidden" name="catid" value="<?php echo $catid ?>">
<input type="submit" value="<?php
if (isset($_GET["stockid"]))
	echo "Edit stock";
else
	echo "Create stock";
?>">
</form>
<p>
<div class="button"><a href="browsecat.php?catid=<?php echo $catid; ?>">Back to Menu</a></div>
</body>
</html>
